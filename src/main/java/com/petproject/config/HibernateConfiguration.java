package com.petproject.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @author Andrey Sorokin <sorokin1990@gmail.com>
 * @since 16.12.2015.
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
        basePackages = {"com.petproject.business"},
        entityManagerFactoryRef = "entityManagerFactory",
        transactionManagerRef = "transactionManager")
public class HibernateConfiguration {
    private static final String PROPERTY_DATABASE_DRIVER = "org.postgresql.Driver";
    private static final String PROPERTY_DATABASE_URL = "jdbc:postgresql://localhost:5432/theroof";
    private static final String PROPERTY_DATABASE_USERNAME = "petProject";
    private static final String PROPERTY_DATABASE_PASSWORD = "1234";
    private static final String PROPERTY_HIBERNATE_DIALECT = "org.hibernate.dialect.PostgreSQLDialect";
    private static final String PROPERTY_HIBERNATE_SHOW_SQL = "true";
    private static final String PROPERTY_HIBERNATE_FORMAT_SQL = "true";
    private static final String PROPERTY_ENTITY_MANAGER_PACKAGES_TO_SCAN = "com.petproject.business.models";
    private static final String PROPERTY_HBM2DLL_STRATEGY = "create-drop";

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();

        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(additionalProperties());
        em.setDataSource(dataSource());
        em.setPackagesToScan(PROPERTY_ENTITY_MANAGER_PACKAGES_TO_SCAN);

        return em;
    }

    @Bean
    public DataSource dataSource() {
        System.out.println("--------------");
        System.out.println("Data Source Initialization");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(PROPERTY_DATABASE_DRIVER);
        dataSource.setUrl(PROPERTY_DATABASE_URL);
        dataSource.setUsername(PROPERTY_DATABASE_USERNAME);
        dataSource.setPassword(PROPERTY_DATABASE_PASSWORD);
        System.out.println("--------------");
        System.out.println("Initialized");
        try {
            System.out.println(dataSource.getConnection());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dataSource;
    }

    @Bean
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().nativeEntityManagerFactory);

        return transactionManager;
    }

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    Properties additionalProperties() {
        return new Properties() {
            {
                setProperty("hibernate.hbm2ddl.auto", PROPERTY_HBM2DLL_STRATEGY);
                setProperty("hibernate.dialect", PROPERTY_HIBERNATE_DIALECT);
                setProperty("hibernate.globally_quoted_identifiers", "true");
                setProperty("hibernate.show_sql", PROPERTY_HIBERNATE_SHOW_SQL);
                setProperty("hibernate.format_sql", PROPERTY_HIBERNATE_FORMAT_SQL);
            }
        };
    }
}