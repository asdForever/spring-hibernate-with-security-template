package com.petproject.initializers;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Andrey Sorokin <sorokin1990@gmail.com>
 * @since 01.03.2016
 */
public class SpringSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
