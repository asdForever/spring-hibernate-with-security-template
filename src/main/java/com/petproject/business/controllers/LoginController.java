package com.petproject.business.controllers;

import com.petproject.business.service.interfaces.PersonService;
import com.petproject.business.models.PersonEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Andrey Sorokin <sorokin1990@gmail.com>
 * @since 21.11.2015
 */
@Controller
public class LoginController {

    @Resource(name="personService")
    private PersonService personService;

    @RequestMapping(value = { "/" }, method = RequestMethod.GET)
    public String homePage(Model model) {
        model.addAttribute("greeting", "Hi, Welcome to mysite. ");
        return "welcome";
    }

    @RequestMapping(value = { "/addPerson" }, method = RequestMethod.GET)
    public String addPerson() {
        PersonEntity person = new PersonEntity("Andrey", "Sorokin", (double) 100);
        personService.addPerson(person);
        return "redirect:/";
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public String adminPage() {
        return "admin";
    }

    @RequestMapping(value = "/db", method = RequestMethod.GET)
    public String dbaPage() {
        return "dba";
    }

    @RequestMapping(value="/logout", method = RequestMethod.GET)
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/Access_Denied", method = RequestMethod.GET)
    public String accessDeniedPage() {
        return "accessDenied";
    }
}
