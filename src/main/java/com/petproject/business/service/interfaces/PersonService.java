package com.petproject.business.service.interfaces;

import com.petproject.business.models.PersonEntity;

import java.util.List;

/**
 * @author Andrey Sorokin <sorokin1990@gmail.com>
 * @since 16.12.2015.
 */
public interface PersonService {
    List<PersonEntity> getAllPersons();

    void addPerson(PersonEntity person);
}