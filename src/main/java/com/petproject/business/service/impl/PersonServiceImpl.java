package com.petproject.business.service.impl;

import com.petproject.business.service.interfaces.PersonService;
import com.petproject.business.dao.interfaces.PersonDAO;
import com.petproject.business.models.PersonEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Andrey Sorokin <sorokin1990@gmail.com>
 * @since 17.12.2015.
 */
@Service("personService")
@Transactional
public class PersonServiceImpl implements PersonService {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    PersonDAO personDAO;

    @Override
    public List<PersonEntity> getAllPersons() {
        return personDAO.getAllPersons();
    }

    @Override
    public void addPerson(PersonEntity person) {
        personDAO.addPerson(person);
    }
}
