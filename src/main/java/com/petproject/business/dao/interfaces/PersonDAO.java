package com.petproject.business.dao.interfaces;

import com.petproject.business.models.PersonEntity;

import java.util.List;

/**
 * @author Andrey Sorokin <sorokin1990@gmail.com>
 * @since 16.12.2015.
 */
public interface PersonDAO {
    List<PersonEntity> getAllPersons();

    void addPerson(PersonEntity person);
}
