package com.petproject.business.dao.impl;

import com.petproject.business.dao.interfaces.PersonDAO;
import com.petproject.business.models.PersonEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author Andrey Sorokin <sorokin1990@gmail.com>
 * @since 16.12.2015.
 */
@Repository
@Transactional
public class PersonDAOImpl implements PersonDAO {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<PersonEntity> getAllPersons() {
        List<PersonEntity> employees = entityManager.createQuery("Select a From PersonEntity a", PersonEntity.class).getResultList();
        return employees;
    }

    @Override
    public void addPerson(PersonEntity person) {
        entityManager.persist(person);
    }
}
